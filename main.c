#include "SDL2_template.h"
#include "SDL2_grid.h"

#define ROWS 4
#define COLS 4
#define CW 100
#define CH 100
#define GRID_OFFSET 1
#define OFFSET 5

enum {EMPTY, XFILL, CROSSHATCH};
grid_t g;
int  cell_state[ROWS][COLS];

int main(int argc,char* argv[])
{
    (void) argc;
    (void) argv;

    SetWidthHeigth(800,600);
    SetTitle("SDL2 grid test");
    SetBackground(&black);

    InitGraph();
    atexit(CloseGraph);

    SetOffset(&g,OFFSET);

    Loop();

    return 0;
}

void HandleKeyPress(SDL_Event e)
{
    (void) e;
    /* Use e.key.keysym.sym */
}

void HandleMouse(SDL_Event e)
{
    int x,y;
    size_t row,col;
    if(e.button.button == SDL_BUTTON_LEFT)
    {
        SDL_GetMouseState(&x,&y);
        if(GetCell(&g,x,y,&row,&col))
        {
            if(cell_state[row][col] != XFILL)
                cell_state[row][col] = XFILL;
            else
                cell_state[row][col] = EMPTY;
        }
    }
    if(e.button.button == SDL_BUTTON_RIGHT)
    {
        SDL_GetMouseState(&x,&y);
        if(GetCell(&g,x,y,&row,&col))
        {
            if(cell_state[row][col] != CROSSHATCH)
                cell_state[row][col] = CROSSHATCH;
            else
                cell_state[row][col] = EMPTY;
        }
    }
}

void Draw()
{
    InitGrid(&g,COLS,ROWS,(width-GRID_OFFSET)/COLS,(heigth-GRID_OFFSET)/ROWS,&maroon);
    SDL_Point P={(width-GridPixelWidth(&g))/2,(heigth-GridPixelHeigth(&g))/2};
    SetCorner(&g,&P);
    DrawGrid(&g);
    for(size_t i=0;i<ROWS;++i)
        for(size_t j=0;j<COLS;++j)
        {
            switch(cell_state[i][j])
            {
                case EMPTY:
                    FillCell(&g,i,j,&brown);
                    SDL_Point M; // middle cell point
                    GetCellMiddle(&g,i,j,&M);
                    SDL_SetRenderDrawColor(renderer,white.r,white.g,white.b,white.a);
                    SDL_RenderDrawPoint(renderer,M.x,M.y);
                    break;
                case XFILL:
                    XCell(&g,i,j,&green);
                    break;
                case CROSSHATCH:
                    HatchFillCell(&g,i,j,&green,10);
                    break;
            }
        }
}
