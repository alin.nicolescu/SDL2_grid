# SDL2 Grid app

Basic `SDL2` app written in `C` to demonstrate the use of a grid.

![](grid-test.png)

The `*.h` files are *single file header libraries* for `C`.

## References

- [https://www.libsdl.org/](https://www.libsdl.org/)
- [Single file library howto](https://github.com/nothings/stb/blob/master/docs/stb_howto.txt)
- [https://github.com/nothings/stb](https://github.com/nothings/stb)

## Usage

- Press `q` to quit
- Use a `left` mouse click to *X fill* a cell or undo the *X fill*
- Use a `right` mouse click to *cross hatch fill* a cell or undo the *cross
  hatch fill*

## Build and run

Tested on:

- `Linux` (Debian and Arch)
- `FreeBSD`
- `Windows`

### Linux and FreeBSD

```console
make && ./grid
```

See the `Makefile` for build details.

### Windows

Use the `winmake.bat` file.

The app compiles fine with [MinGW](https://sourceforge.net/projects/mingw/)
assuming you set the *path environment variable*:

```
mingw32-make.exe -f WinMakefile
```

Change the following lines from `WinMakefile`  if you installed `SDL2` in
other location:

```
INCLUDE_PATH = C:\MinGW\mingw64\include
LIBS_PATH = C:\MinGW\mingw64\libs
```

