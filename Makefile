CC = cc

# CC_FLAGS = -Wall -Wextra -ggdb
CC_FLAGS = -Wall -Wextra
SDL2_FLAGS = `sdl2-config --cflags`
CC_LIBS = -lm
# SDL2_LIBS = `sdl2-config --libs` -lSDL2_gfx -lSDL2_ttf
SDL2_LIBS = `sdl2-config --libs`

FLAGS = $(CC_FLAGS) $(SDL2_FLAGS)
LIBS = $(SDL2_LIBS) $(CC_LIBS)

TARGET = grid
OBJ = main.o SDL2_template.o SDL2_grid.o

$(TARGET): $(OBJ)
	$(CC) -o $(TARGET) $(FLAGS) $(OBJ) $(LIBS) 

SDL2_template.o : SDL2_template.h
	$(CC) -DSDL2_TEMPLATE_IMPLEMENTATION $(FLAGS) -x c -c SDL2_template.h

SDL2_grid.o : SDL2_grid.h
	$(CC) -DSDL2_GRID_IMPLEMENTATION $(FLAGS) -x c -c SDL2_grid.h

main.o: main.c
	$(CC) $(FLAGS) -c main.c

.PHONY: clean
clean:
	rm -fv $(TARGET) $(OBJ)
